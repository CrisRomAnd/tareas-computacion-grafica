# Practica 7: Iluminación


## Dependencias:

	* OpenGL >= 3.0
	* GLEW >= 2.1.0
	* GLFW3
	
## Compilacion

Para compilar: `make`

Para compilar y ejecutar: `make run`

Para limpiar binarios: `make clean`

### Notas

En [shader_light.fs](shaders/shader_light.fs "Archivo") están  implementadas las ecuaciónes de Lambert

La unica diferencia entre este código y el del VS es que cambie los includes en [esfera.h](esfera.h)
y elimine el valor de PI ya que gcc tomo automaticamente el de la biblioteca math

```c++
#include <glew.h>
#include <glfw3.h>
```

por


```c++
#include <GL/glew.h>
#include <GLFW/glfw3.h>
```
