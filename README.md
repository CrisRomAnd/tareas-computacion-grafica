# Tareas y prácticas de computación gŕafica

## Configuración en Linux

### Dependencias

	* opengl >= 3.1
	* glew >= 2.1
	* glfw3
	
Estas dependencias se pueden checar con el comando
			
```shell
$ glxinfo | grep "OpenGL version"
OpenGL version 4.5 (Compatibility Profile) Mesa 20.0.4 is supported
```
	
La versión debe ser mayor a 3.1.0 para asignar ese valor
se debe sobreescribe el archivo /etc/environment

Como usiario administrador:

```shell
$ echo "MESA_GL_VERSION_OVERRIDE=4.5" >> /etc/environment && \ 
	echo "MESA_GLSL_VERSION_OVERRIDE=450" >> /etc/environment
```

Verificar si la targeta de vídeo soporta dicha versión,
se puede verificar las versiones soportadas con el siguiente comando

```shell
$ glxinfo | grep -i opengl
OpenGL vendor string: Intel Open Source Technology Center
OpenGL renderer string: Mesa DRI Intel(R) HD Graphics 4000 (IVB GT2)
OpenGL core profile version string: 4.5 (Core Profile) Mesa 20.0.4
OpenGL core profile shading language version string: 4.50
OpenGL core profile context flags: (none)
OpenGL core profile profile mask: core profile
OpenGL core profile extensions:
OpenGL version string: 4.5 (Compatibility Profile) Mesa 20.0.4
OpenGL shading language version string: 4.50
OpenGL context flags: (none)
OpenGL profile mask: compatibility profile
OpenGL extensions:
OpenGL ES profile version string: OpenGL ES 3.0 Mesa 20.0.4
OpenGL ES profile shading language version string: OpenGL ES GLSL ES 3.00
```

En este caso soporta la version 4.5
