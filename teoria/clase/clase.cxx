/*---------------------------------------------------------*/
/* ----------------   Pr�ctica 3 --------------------------*/
/*-----------------    2020-2   ---------------------------*/
/*-------Alumno:							 ---------------*/
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <shader_m.h>

#include <iostream>
#include <stack>


void resize(GLFWwindow* window, int width, int height);
void my_input(GLFWwindow *window);

// settings
// Window size
int SCR_WIDTH = 3800;
int SCR_HEIGHT = 7600;

GLFWmonitor *monitors;
GLuint VBO, VAO, EBO;

void myData(void);
void display(Shader);
void getResolution(void);

//For Keyboard
float	movX = 0.0f,
		movY = 0.0f,
		movZ = -5.0f;

void getResolution()
{
	const GLFWvidmode * mode = glfwGetVideoMode(glfwGetPrimaryMonitor());

	SCR_WIDTH = mode->width;
	SCR_HEIGHT = (mode->height) - 80;
}

void myData()
{	
		GLfloat vertices[] = {
		//Position				//Color
		0.0f, 0.0f, 0.5f,		1.0f, 0.0f, 0.0f,	//V0
		2.0f, 0.0f, 0.5f,		1.0f, 0.0f, 0.0f,	//V1
		1.5f, 1.0f, 0.5f,		1.0f, 0.0f, 0.0f,	//V2
		0.5f, 1.0f, 0.5f,		1.0f, 0.0f, 0.0f,	//V3 Trapecio

		0.00f, 0.00f, 0.5f,		1.0f, 1.0f, 0.0f,	//V4
		
		0.16f, 0.47f, 0.5f,		1.0f, 1.0f, 0.0f,	//V5
		0.36f, 0.35f, 0.5f,		1.0f, 1.0f, 0.0f,	//V6
	  0.47f, 0.16f, 0.5f,		1.0f, 1.0f, 0.0f,	//V7
		0.50f, 0.00f, 0.5f,		1.0f, 1.0f, 0.0f,	//V8

		0.47f, -0.16f, 0.5f,		1.0f, 1.0f, 0.0f,	//V09
		0.36f, -0.35f, 0.5f,		1.0f, 1.0f, 0.0f,	//V10
		0.16f, -0.47f, 0.5f,		1.0f, 1.0f, 0.0f,	//V11
		
		-0.16f, -0.47f, 0.5f,		1.0f, 1.0f, 0.0f,	//V12
		-0.36f, -0.35f, 0.5f,		1.0f, 1.0f, 0.0f,	//V13
		-0.47f, -0.16f, 0.5f,		1.0f, 1.0f, 0.0f,	//V14
		-0.50f, -0.0f, 0.5f,		1.0f, 1.0f, 0.0f,	//V15

		-0.47f, 0.16f, 0.5f,		1.0f, 1.0f, 0.0f,	//V16
		-0.36f, 0.35f, 0.5f,		1.0f, 1.0f, 0.0f,	//V17
		-0.16f, 0.47f, 0.5f,		1.0f, 1.0f, 0.0f,	//V18
		0.16f, 0.47f, 0.5f,		1.0f, 1.0f, 0.0f,	//V18


 		0.0f, 0.0f, 0.5f,		1.0f, 0.0f, 0.0f,	//V19
 		1.0f, 0.0f, 0.5f,		1.0f, 0.0f, 0.0f,	//V20
		0.5f, 0.86f, 0.5f,		1.0f, 0.0f, 0.0f,	//V21

		0.0f, 0.0f, 0.5f,		1.0f, 1.0f, 0.0f,	//V22
 		5.0f, 0.0f, 0.5f,		1.0f, 1.0f, 0.0f,	//V23
		5.0f, 7.0f, 0.5f,		1.0f, 1.0f, 0.0f,	//V25
		0.0f, 7.0f, 0.5f,		1.0f, 1.0f, 0.0f,	//V24
		



	};

	unsigned int indices[] =	//I am not using index for this session
	{
	 0,1,2,3,//4
	 4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,4,//21
	 19,20,21,//24
	 22,23,24,25

	};
glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	glGenBuffers(1, &VBO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	// position attribute
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	// color attribute
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);

	//Para trabajar con indices (Element Buffer Object)
	glGenBuffers(1, &EBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);


}

void display(Shader shader)
{
	//Shader myShader("shaders/shader.vs", "shaders/shader.fs");
	Shader projectionShader("shaders/shader_projection.vs", "shaders/shader_projection.fs");

	projectionShader.use();

	// create transformations and Projection
	glm::mat4 model = glm::mat4(1.0f);		// initialize Matrix, Use this matrix for individual models
	glm::mat4 view = glm::mat4(1.0f);		//Use this matrix for ALL models
	glm::mat4 projection = glm::mat4(1.0f);	//This matrix is for Projection

	// projection = glm::perspective(glm::radians(45.0f), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
	projection = glm::ortho(-10.0f, 10.0f, -4.0f, 4.0f, 0.1f, 10.0f);

	//Use "view" in order to affect all models
	view = glm::translate(view, glm::vec3(movX, movY, movZ));
	// pass them to the shaders
	projectionShader.setVec3("aColor", glm::vec3(1.0f, 0.6f, 0.0f));
	projectionShader.setMat4("model", model);
	projectionShader.setMat4("view", view);
	// note: currently we set the projection matrix each frame, but since the projection matrix rarely changes it's often best practice to set it outside the main loop only once.
	projectionShader.setMat4("projection", projection);

	glBindVertexArray(VAO);
	
	projectionShader.setVec3("aColor", glm::vec3(1.0f, 1.0f, 0.0f));
	model = glm::translate(model, glm::vec3(0.0f,-3.5f,0.0f));
	model = glm::scale(model, glm::vec3(2.0f,3.0f,1.0f));
	projectionShader.setMat4("model", model);
	projectionShader.setVec3("aColor", glm::vec3(1.0f, 0.0f, 1.0f));
	glDrawArrays(GL_QUADS, 0, 4); // trapecio
	
	model = glm::translate(glm::mat4(4.0f), glm::vec3(1.75f, 3.95f,1.0f));
	projectionShader.setMat4("model", model);
	projectionShader.setVec3("aColor", glm::vec3(1.0f, 0.0f, 1.0f));
	glDrawArrays(GL_TRIANGLE_FAN, 4, 16);// circulo
	
	model = glm::translate(glm::mat4(4.0f), glm::vec3(1.5f, -0.5f,0.0f));
	model = glm::scale(model, glm::vec3(0.1f,0.57f,1.0f));
	projectionShader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 24, 4);//  rectangulo
	
	glm::mat4 triangles = glm::mat4(1.0f);
	triangles = model = glm::translate(triangles, glm::vec3(1.23f,3.1f,0.5f));
	projectionShader.setMat4("model", model);
	projectionShader.setVec3("aColor", glm::vec3(1.0f, 0.0f, 1.0f));
	glDrawArrays(GL_TRIANGLES, 21, 3);// triangulo

	triangles = model = glm::translate(triangles, glm::vec3(1.0f,1.7f,0.0f));
	model = glm::rotate(model, glm::radians(180.0f), glm::vec3(0.0f,0.0f,1.0f));
	projectionShader.setMat4("model", model);
	projectionShader.setVec3("aColor", glm::vec3(1.0f, 0.0f, 1.0f));
	glDrawArrays(GL_TRIANGLES, 21, 3);// triangulo
	
	model = glm::translate(triangles, glm::vec3(-1.36f,-1.35f,0.0f));
	model = glm::rotate(model, glm::radians(30.0f), glm::vec3(0.0f,0.0f,1.0f));
	projectionShader.setMat4("model", model);
	projectionShader.setVec3("aColor", glm::vec3(1.0f, 0.0f, 1.0f));
	glDrawArrays(GL_TRIANGLES, 21, 3);// triangulo


	model = glm::rotate(model, glm::radians(180.0f), glm::vec3(0.0f,0.0f,1.0f));
	model = glm::translate(model, glm::vec3(-2.0f,0.0f,0.0f));
	projectionShader.setMat4("model", model);
	projectionShader.setVec3("aColor", glm::vec3(1.0f, 0.0f, 1.0f));
	glDrawArrays(GL_TRIANGLES, 21, 3);// triangulo
	

	

	// shader.setVec3("aColor", glm::vec3(1.0f, 1.0f, 1.0f));
	// glDrawArrays(GL_TRIANGLES, 21, 3);// triangulo
	// glBindVertexArray(0);

	// glBindVertexArray(VAO);
	// shader.setVec3("aColor", glm::vec3(1.0f, 1.0f, 0.0f));
	// glDrawArrays(GL_QUADS, 24, 4);//  rectangulo
	// glBindVertexArray(0);

	glBindVertexArray(0);

}

int main()
{
    // glfw: initialize and configure
    // ------------------------------
    glfwInit();
    /*glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);*/

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // uncomment this statement to fix compilation on OS X
#endif

    // glfw window creation
    // --------------------
	monitors = glfwGetPrimaryMonitor();
	getResolution();

    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "Programa 2: Molino", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
	glfwSetWindowPos(window, 0, 30);
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, resize);

	glewInit();


	//Mis funciones
	//Datos a utilizar
	myData();
	glEnable(GL_DEPTH_TEST);

	Shader projectionShader("shaders/shader_projection.vs", "shaders/shader_projection.fs");

    // render loop
    // While the windows is not closed
    while (!glfwWindowShouldClose(window))
    {
        // input
        // -----
        my_input(window);

        // render
        // Backgound color
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//Mi funci�n de dibujo
		display(projectionShader);

        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        // -------------------------------------------------------------------------------
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    // glfw: terminate, clearing all previously allocated GLFW resources.
    // ------------------------------------------------------------------
    glfwTerminate();
    return 0;
}

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void my_input(GLFWwindow *window)
{
    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)  //GLFW_RELEASE
        glfwSetWindowShouldClose(window, true);
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
		movX += 0.08f;
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
		movX -= 0.08f;
	if (glfwGetKey(window, GLFW_KEY_PAGE_UP) == GLFW_PRESS)
		movY += 0.08f;
	if (glfwGetKey(window, GLFW_KEY_PAGE_DOWN) == GLFW_PRESS)
		movY -= 0.08f;
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		movZ -= 0.08f;
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		movZ += 0.08f;

}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void resize(GLFWwindow* window, int width, int height)
{
    // Set the Viewport to the size of the created window
    glViewport(0, 0, width, height);
}
