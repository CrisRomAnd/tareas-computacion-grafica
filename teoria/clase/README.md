Programa 2: Molino
---

Alumno: Romero Andrade Cristian

## Compilación

```shell
make
```

o bien 

```shell
g++ -std=c++11 ventilador.cxx -lGLEW -lGLU -lm -lGL -lglfw -lrt -lm -ldl -lX11 -lpthread -lxcb -lXau -lXdmcp -I include/ -o ventilador.out
```

![Ventilador](Ventilador.png "Ventilador")
